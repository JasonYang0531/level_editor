public class Dungeon_Model  {
	private final int BOARD_SIZE = 20;
	private final char UNWALKABLE = 'u';
	private final char WALKABLE = 'w';
	private final char CHARACTER = 'c';
	private int characterRow = BOARD_SIZE/2;
	private int characterCol = BOARD_SIZE/2;
	private char[][] board;
	
	public Dungeon_Model() {
		board = new char[BOARD_SIZE][BOARD_SIZE];
		//Hard coding level simply for proof of concept, we will make the levels different later
		//For proof of concept, we are only making a first level where the user can walk around.
		//No monsters or NPC's will exist.
		
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				board[i][j] = WALKABLE;
			}
		}
		for (int i = 0; i < board.length; i++) {
			board[i][0] = UNWALKABLE;
			board[i][19] = UNWALKABLE;
		}
		for (int i = 0; i < board[0].length; i++) {
			board[0][i] = UNWALKABLE;
			board[19][i] = UNWALKABLE;
		}
		board[BOARD_SIZE/2][BOARD_SIZE/2] = CHARACTER;
	}
	public void moveCharacter(int dir) {
		// dir shows direction to move
		// 1 is up
		// 2 is right
		// 3 is down
		// 4 is left
		if (dir == 1) {
			if (isInBounds(characterRow-1, characterCol) && isWalkable(characterRow-1, characterCol)) {
				board[characterRow-1][characterCol] = CHARACTER;
				board[characterRow][characterCol] = WALKABLE;
				updateCharacterInfo(characterRow-1, characterCol);
			}
		}
		else if (dir == 2) {
			if (isInBounds(characterRow, characterCol+1) && isWalkable(characterRow, characterCol+1)) {
				board[characterRow][characterCol+1] = CHARACTER;
				board[characterRow][characterCol] = WALKABLE;
				updateCharacterInfo(characterRow, characterCol+1);
			}
			
		}
		else if (dir == 3) {
			if (isInBounds(characterRow+1, characterCol) && isWalkable(characterRow+1, characterCol)) {
				board[characterRow+1][characterCol] = CHARACTER;
				board[characterRow][characterCol] = WALKABLE;
				updateCharacterInfo(characterRow+1, characterCol);
			}
			
		}
		else if (dir == 4) {
			if (isInBounds(characterRow, characterCol-1) && isWalkable(characterRow, characterCol-1)) {
				board[characterRow][characterCol-1] = CHARACTER;
				board[characterRow][characterCol] = WALKABLE;
				updateCharacterInfo(characterRow, characterCol-1);
			}
			
		}
		
	}
	
	private boolean isInBounds(int row, int col) {
		if (row >= 0 && row < 20) {
			if (col >= 0 && col < 20) {
				return true;
			}
		}
		return false;
	}
	private boolean isWalkable(int row, int col) {
		if (board[row][col] == WALKABLE) {
			return true;
		}
		return false;
	}

	private void updateCharacterInfo(int row, int col) {
		characterRow = row;
		characterCol = col;
	}
	
	public char[][] getBoard() {
		return board;
	}
	
	public int numCols() {
		return BOARD_SIZE;
	}
	
	public int numRows() {
		return BOARD_SIZE;
	}
}
