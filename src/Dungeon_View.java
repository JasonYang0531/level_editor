import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Dungeon_View extends JFrame{
	
	private GamePanel gamePanel;

	public Dungeon_View(Dungeon_Controller controller) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Dungeon Crawler");
		setBounds(0, 0, 600, 600);
		setLayout(new FlowLayout());
		gamePanel = new GamePanel(controller);
		gamePanel.setPreferredSize(new Dimension(400, 400));
		gamePanel.setBorder(new LineBorder(Color.BLACK, 1));
		this.add(gamePanel);
		this.setVisible(true);;
	}	
	
	public void addCustomKeyListener(KeyListener k) {
	     this.addKeyListener(k);
	}
	
	public class GamePanel extends JPanel {
		private static final long serialVersionUID = -7643413096529405862L;
		private Dungeon_Controller controller;
		
		public GamePanel(Dungeon_Controller controller) {
			this.controller = controller;
		}	
		
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			drawBackground(g);
		}

		/** Draws the main background tiles */
		private void drawBackground(Graphics g) {
			int cellWidth = cellWidth();
			int cellHeight = cellHeight();
			
			for (int r = 0; r < controller.numRows(); r++) {
				for (int c = 0; c < controller.numCols(); c++) {
					BufferedImage img = controller.getBackgroundImageAt(r, c);
					if (img != null)
						g.drawImage(img, c*cellWidth, r*cellHeight, cellWidth, cellHeight, null);
				}
			}
		}
		
		/** Cell width in pixels */
		private int cellWidth() {
			return getWidth() / controller.numCols();
		}

		/** Cell height in pixels */
		private int cellHeight() {
			return getHeight() / controller.numRows();
		}
	}
}
